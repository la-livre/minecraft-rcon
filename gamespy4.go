package minecraft

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"net"
	"strconv"
	"time"
)

const (
	packetTypeHandshake = '\x09'
	packetTypeStats     = '\x00'
	sessionID           = 253
)

var (
	nullSep          = make([]byte, 1) // The separator needs to be a list, so here's a list with one null byte I guess
	packetMagic      = []byte{'\xFE', '\xFD'}
	paddingFullStats = []byte{'\x01', '\x70', '\x6C', '\x61', '\x79', '\x65', '\x72', '\x5F', '\x00', '\x00'}
)

type outPacket struct {
	Type      byte
	SessionID uint32
	Token     []byte
	Full      bool
}

func (out outPacket) toBytes() []byte {
	sessionBytes := make([]byte, 4)
	binary.BigEndian.PutUint32(sessionBytes, out.SessionID&0x0F0F0F0F)

	var final = append(packetMagic[:], out.Type)
	final = append(final[:], sessionBytes[:]...)
	if out.Token != nil {
		final = append(final[:], out.Token[:]...)
	}
	if out.Full {
		final = append(final[:], make([]byte, 4)...)
	}
	return final
}

type inPacket struct {
	Type      byte
	SessionID uint32
	Payload   []byte
}

func (in *inPacket) New(p []byte) {
	in.Type = p[0]
	in.SessionID = binary.BigEndian.Uint32(p[1:5])
	in.Payload = p[5:]
}

// MinStats contains the parsed data returned by a normal server query
type MinStats struct {
	MODT       string
	GameType   string
	Map        string
	NumPlayers int
	MaxPlayers int
	HostPort   uint16
	HostIP     string
}

// New will populate stats' fields from the raw data p
func (stats *MinStats) New(p []byte) error {
	subs := bytes.SplitN(p, nullSep, 7) // The last one is just to avoid having to trim HostIP

	stats.MODT = string(subs[0])
	stats.GameType = string(subs[1])
	stats.Map = string(subs[2])

	// I could just keep them as strings, but for I just want to keep numbers as numbers, for consistency
	// *Looking at you, HostPort*
	var err error
	stats.NumPlayers, err = strconv.Atoi(string(subs[3]))
	if err != nil {
		return err
	}
	stats.MaxPlayers, err = strconv.Atoi(string(subs[4]))
	if err != nil {
		return err
	}

	// Sub5 contains both HostPort and HostIP. Hostports being 2 bytes wide, here's some maths
	stats.HostPort = binary.LittleEndian.Uint16(subs[5][:2])
	stats.HostIP = string(subs[5][2:])

	return nil
}

// FullStats contains the parsed data returned by a detailed server query
type FullStats struct {
	Hostname   string // Actually MOTD, but I want to keep consistency with
	GameType   string
	GameID     string
	Version    string
	Plugins    string
	Map        string
	NumPlayers int
	MaxPlayers int
	HostPort   int
	HostIP     string
	Players    []string
}

// New will populate stats' fields from the raw data p
func (stats *FullStats) New(p []byte) error {
	subs := bytes.Split(p, nullSep)

	var index int
	var err error
	for {
		if bytes.Compare(subs[index], []byte("hostname")) == 0 {
			stats.Hostname = string(subs[index+1])
		} else if bytes.Compare(subs[index], []byte("gametype")) == 0 {
			stats.GameType = string(subs[index+1])
		} else if bytes.Compare(subs[index], []byte("game_id")) == 0 {
			stats.GameID = string(subs[index+1])
		} else if bytes.Compare(subs[index], []byte("version")) == 0 {
			stats.Version = string(subs[index+1])
		} else if bytes.Compare(subs[index], []byte("plugins")) == 0 {
			stats.Plugins = string(subs[index+1])
		} else if bytes.Compare(subs[index], []byte("map")) == 0 {
			stats.Map = string(subs[index+1])
		} else if bytes.Compare(subs[index], []byte("numplayers")) == 0 {
			stats.NumPlayers, err = strconv.Atoi(string(subs[index+1]))
			if err != nil {
				return err
			}
		} else if bytes.Compare(subs[index], []byte("maxplayers")) == 0 {
			stats.MaxPlayers, err = strconv.Atoi(string(subs[index+1]))
			if err != nil {
				return err
			}
		} else if bytes.Compare(subs[index], []byte("hostport")) == 0 {
			stats.HostPort, err = strconv.Atoi(string(subs[index+1]))
			if err != nil {
				return err
			}
		} else if bytes.Compare(subs[index], []byte("hostip")) == 0 {
			stats.HostIP = string(subs[index+1])
		} else {
			break
		}
		index += 2 // It starts from 0, an even numbers. Even numbers are keys, the following odd ones are the value associated, hence the index+1
	}
	index = bytes.LastIndex(p, paddingFullStats) + 10
	players := bytes.Split(p[index:], nullSep)
	for _, player := range players {
		if string(player) == "" {
			break
		} else {
			stats.Players = append(stats.Players, string(player))
		}
	}
	return nil
}

// GetServerFullStats returns server detailed query
func GetServerFullStats(IP string) (FullStats, error) {
	payload, err := establishConnection(IP, true)
	if err != nil {
		return FullStats{}, err
	}
	var full FullStats
	err = full.New(payload[11:])
	return full, err
}

// GetServerMinStats returns server normal query
func GetServerMinStats(IP string) (MinStats, error) {
	payload, err := establishConnection(IP, false)
	if err != nil {
		return MinStats{}, err
	}
	var min MinStats
	err = min.New(payload[5:])
	return min, err
}

func establishConnection(IP string, full bool) ([]byte, error) {
	address := IP
	raddr, err := net.ResolveUDPAddr("udp", address)
	if err != nil {
		panic(err)
	}
	conn, err := net.DialUDP("udp", nil, raddr)
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	conn.SetReadDeadline(time.Now().Add(5 * time.Second))

	challengeResponse := make([]byte, 256) // The response is actually a lot smaller
	outChallenge := outPacket{
		Type:      packetTypeHandshake,
		SessionID: sessionID,
	}
	var tried bool
GET_FLAG:
	conn.Write(outChallenge.toBytes())
	_, err = bufio.NewReader(conn).Read(challengeResponse)
	if err != nil {
		return nil, err
	}
	var inChallenge inPacket
	inChallenge.New(challengeResponse)
	tokenInt, err := strconv.Atoi(string(inChallenge.Payload[:strlen(inChallenge.Payload)]))
	if err != nil {
		return nil, err
	}
	tokenBytes := make([]byte, 4)
	token := uint32(tokenInt)
	binary.BigEndian.PutUint32(tokenBytes, token)
	statsResponse := make([]byte, 65535) // Just to be sure
	outStats := outPacket{
		Type:      packetTypeStats,
		SessionID: outChallenge.SessionID,
		Token:     tokenBytes,
		Full:      full,
	}
	conn.Write(outStats.toBytes())
	_, err = bufio.NewReader(conn).Read(statsResponse)
	if err != nil && tried == false { // Minecraft has a weird timer logic, so allow for a second retry. It deletes all token every 30 seconds, no matter if they have just been created or not, so you might request a token and have it deleted by the next second because you entered a new 30-seconds window in between
		tried = true
		goto GET_FLAG
	} else if err != nil {
		panic(err)
	}
	var inStats inPacket
	inStats.New(statsResponse)
	return inStats.Payload, nil
}

func strlen(bytes []byte) int {
	for i := range bytes {
		if bytes[i] != 0 {
		} else {
			return i
		}
	}
	return -1
}
