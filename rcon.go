package minecraft

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"errors"
	"io"
	"net"
	"sync"
	"time"
)

const (
	rconPacketTypeLogin      int32 = 3
	rconPacketTypeCommandReq int32 = 2
)

// ErrNotAuthentificated is used to indicate that the connection is not authentificated yet
var ErrNotAuthentificated = errors.New("Connection is not authentificated. No commands can be issued")

// ErrAuthFailed is used to indicate that the password has seemingly been rejected
var ErrAuthFailed = errors.New("Server responded but refused authentification with provided password")

// RCONConnection holds infos about an RCON connection
type RCONConnection struct {
	authed     bool
	conn       *net.TCPConn
	lastSent   time.Time
	sync.Mutex // While concurrency isn't implemented, RCONConnection will at least be thread safe
}

// SendCommand will send a command and will returns what the RCON servers sends back and if an error occured
// NOTE: The RCON server doesn't always return something, so detecting unknown commands is impossible
func (c *RCONConnection) SendCommand(command string) (string, error) {
	c.conn.SetReadDeadline(time.Now().Add(5 * time.Second))
	c.Lock()
	defer c.Unlock()
	if c.lastSent.Before(time.Now().Add(200 * time.Millisecond)) {
		time.Sleep(200 * time.Millisecond)
	}
	pack := rconPacket{
		RequestID: sessionID,
		Type:      rconPacketTypeCommandReq,
		Payload:   command,
	}
	packBytes, err := pack.ToBytes()
	if err != nil {
		return "", err
	}
	_, err = c.conn.Write(packBytes)
	if err != nil {
		return "", err
	}

	var final []byte
	for { // Handling possible multi-packet responses. I'm doing this wrong, multipacket response are all full fledged packets. I'm stupid
		response := make([]byte, 4096)
		_, err = bufio.NewReader(c.conn).Read(response)
		if err != nil && err != io.EOF {
			return "", err
		}
		final = append(final, response...)
		if response[4094] == 0 && response[4095] == 0 {
			break
		}
	}
	var inPack rconPacket
	if err := inPack.NewIn(final); err != nil {
		return "", err
	}
	return inPack.Payload, nil
}

// New tried to initialize connection, and returns an error if authentification failed or simply couldn't connect to the RCON server
func (c *RCONConnection) New(ip string, password string) error {
	raddr, err := net.ResolveTCPAddr("tcp", ip)
	if err != nil {
		return err
	}
	c.conn, err = net.DialTCP("tcp", nil, raddr)
	if err != nil {
		return err
	}
	c.conn.SetReadDeadline(time.Now().Add(5 * time.Second))

	authPacket := rconPacket{
		Type:      rconPacketTypeLogin,
		RequestID: sessionID,
		Payload:   password,
	}
	authBytes, err := authPacket.ToBytes()
	if err != nil {
		return err
	}

	response := make([]byte, 256)
	_, err = c.conn.Write(authBytes)
	if err != nil {
		return err
	}
	_, err = bufio.NewReader(c.conn).Read(response)
	if err != nil && err != io.EOF {
		return err
	}

	var cPacketResponse rconPacket
	cPacketResponse.NewIn(response)
	if cPacketResponse.RequestID == -1 {
		return ErrAuthFailed
	}
	c.authed = true
	c.lastSent = time.Now()
	return nil
}

// IsAuthed checks if connection has successfully authentificated
func (c *RCONConnection) IsAuthed() bool {
	return c.authed
}

// Close closes net.conn connection
func (c *RCONConnection) Close() error {
	return c.conn.Close()
}

type rconPacket struct {
	length    int32
	RequestID int32
	Type      int32
	Payload   string
}

// ToBytes will convert the struct into a byte array
func (p *rconPacket) ToBytes() ([]byte, error) {
	p.length = int32(len([]byte(p.Payload)) + 10)
	buffer := new(bytes.Buffer)
	binaryWriter := bufio.NewWriterSize(buffer, 12)
	if err := binary.Write(binaryWriter, binary.LittleEndian, p.length); err != nil {
		return nil, err
	}
	if err := binary.Write(binaryWriter, binary.LittleEndian, p.RequestID); err != nil {
		return nil, err
	}
	if err := binary.Write(binaryWriter, binary.LittleEndian, p.Type); err != nil {
		return nil, err
	}
	if err := binaryWriter.Flush(); err != nil {
		return nil, err
	}
	outputBytes := buffer.Bytes()
	outputBytes = append(outputBytes, []byte(p.Payload)...)
	outputBytes = append(outputBytes, []byte{0, 0}...)
	return outputBytes, nil
}

// NewIn will try to pack raw binary data into the packet struct
func (p *rconPacket) NewIn(data []byte) error {
	reader := bytes.NewReader(data)
	if err := binary.Read(reader, binary.LittleEndian, &p.length); err != nil {
		return err
	}
	if err := binary.Read(reader, binary.LittleEndian, &p.RequestID); err != nil {
		return err
	}
	if err := binary.Read(reader, binary.LittleEndian, &p.Type); err != nil {
		return err
	}
	p.Payload = string(data[12 : strlen(data[12:])+12])
	return nil
}
