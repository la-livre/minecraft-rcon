package main

import (
	"encoding/json"
	"fmt"
	minecraft "minecraft-rcon"
	"os"
	"strings"
)

func main() {
	if len(os.Args) == 1 || os.Args[1] == "-h" || os.Args[1] == "--help" {
		fmt.Println("The first argument to the program should be the IP:PORT, the second, optionnal, is the -f/--full flag, which will display more detailed infos. Please note that not providing port will default it to 25565.")
		fmt.Println("Examples:\n./minecraft.exe memenetwork.net -f\n./minecraft.exe memenetwork.net:25565 -f")
		fmt.Println("Please note that not all servers have query enabled, and if the server you try to reach doesn't respond, maybe enable-query is set to false in server.properties or query.port isn't set at all, again, in server.properties.")
		os.Exit(0)
	}
	ip := os.Args[1]
	if strings.Contains(os.Args[1], ":") == false {
		ip += ":25565"
	}
	fmt.Printf("Trying to contact \"%s\"\n", ip)

	var full bool
	if len(os.Args) == 3 && (os.Args[2] == "-f" || os.Args[2] == "--full") {
		full = true
	}

	if full {
		fmt.Println("Detailed mode: On.")
		stats, err := minecraft.GetServerFullStats(ip)
		if err != nil {
			panic(err)
		}
		bytes, err := json.MarshalIndent(stats, "", "\t")
		if err != nil {
			panic(err)
		}
		fmt.Println(string(bytes))
	} else {
		fmt.Println("Detailed mode: Off.")
		stats, err := minecraft.GetServerMinStats(ip)
		if err != nil {
			panic(err)
		}
		bytes, err := json.MarshalIndent(stats, "", "\t")
		if err != nil {
			panic(err)
		}
		fmt.Println(string(bytes))
	}
}
