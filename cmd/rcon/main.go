package main

import (
	"bufio"
	"fmt"
	minecraft "minecraft-rcon"
	"net"
	"os"
	"strings"
)

func main() {
	if len(os.Args) == 1 || os.Args[1] == "-h" || os.Args[1] == "--help" {
		fmt.Println("The first argument to the program should be the IP:PORT, the second should be the password, the third optionnal, is a double quotes-enclosed command, in case you do not want to use the interactive mode. Please note that not providing port will default it to 25575.")
		fmt.Println("\nExamples:\n./minecraft.exe memenetwork.net password\n./minecraft.exe memenetwork.net:25565 password \"time set day\"")
		fmt.Println("\nPlease note that if using interactive mode, commands should not be enquoted")
		fmt.Println("\nPlease note that not all servers have rcon enabled, and if the server you try to reach doesn't respond, maybe enable-rcon is set to false in server.properties or rcon.port and rcon.password aren't set at all, again, in server.properties.")
		os.Exit(0)
	}
	if len(os.Args) < 3 {
		fmt.Println("Not enough aguments")
		os.Exit(-1)
	}

	ip := os.Args[1]
	if strings.Contains(os.Args[1], ":") == false {
		ip += ":25575"
	}
	var conn minecraft.RCONConnection
	if err := conn.New(ip, os.Args[2]); err != nil {
		panic(err)
	}
	fmt.Printf("Authentification to %s succeeded!\n", ip)
	defer conn.Close()
	if len(os.Args) == 4 {
		command := os.Args[3]
		command = strings.TrimSpace(command)
		command = strings.TrimSuffix(command, "\"")
		command = strings.TrimPrefix(command, "\"")
		comm, err := conn.SendCommand(command)
		fmt.Printf("%s:\n%s\n", command, comm)
		if err != nil {
			if err, ok := err.(net.Error); ok && err.Timeout() {
				fmt.Println("Server didn't respond to this command")
			} else {
				panic(err)
			}
		}
	} else {
		scanner := bufio.NewReader(os.Stdin)
		for {
			fmt.Print(">")
			command, _ := scanner.ReadString('\n')
			command = strings.TrimSpace(command)
			comm, err := conn.SendCommand(command)
			fmt.Println(comm)
			if err != nil {
				if err, ok := err.(net.Error); ok && err.Timeout() {
					fmt.Println("Server didn't respond to this command")
				} else {
					panic(err)
				}
			}
		}
	}
}
