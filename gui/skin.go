package main

import (
	"bufio"
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"image"
	"image/png"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/jinzhu/gorm"

	_ "github.com/mattn/go-sqlite3"

	"github.com/nfnt/resize"

	"github.com/oliamb/cutter"
)

// BASEPATH contains the executable's path
var basepath string

const skinLookupTimeout = 20 * time.Minute

type userMinecraft struct {
	gorm.Model
	ID       string
	Name     string
	Avatar   string
	LastRead time.Time
}

func main() {
	db, err := gorm.Open("sqlite3", basepath+"/users.db")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()
	db.AutoMigrate(&userMinecraft{})

	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	basepath = filepath.Dir(ex)
	usrs, err := getUUIDS([]string{"une_livre"})
	if err != nil {
		panic(err)
	}

	for i := range usrs {
		usrs[i], err = downloadSkin(usrs[i])
		if err != nil {
			panic(err)
		}
		fmt.Printf("<p>%s</p><img src=\"data:image/png;base64, %s\" alt=\"%s\"/><br>\n", usrs[i].Name, usrs[i].Avatar, usrs[i].Name)
	}
}

func downloadSkin(user userMinecraft) (userMinecraft, error) {
	url, err := getSkinDownloadURL(user.ID)
	if err != nil {
		return user, err
	}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return user, err
	}
	req.Header.Add("Accept", `text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8`)
	req.Header.Add("User-Agent", `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11`)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return user, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return user, errors.New(http.StatusText(resp.StatusCode))
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return user, err
	}

	img, _, err := image.Decode(bytes.NewReader(bodyBytes))
	if err != nil {
		return user, err
	}

	croppedImg, err := cutter.Crop(img, cutter.Config{
		Width:  8,
		Height: 8,
		Anchor: image.Point{8, 8},
		Mode:   cutter.TopLeft, // optional, default value
	})
	if err != nil {
		return user, err
	}

	var b bytes.Buffer
	foo := bufio.NewWriter(&b)
	if err = png.Encode(foo, resize.Resize(128, 128, croppedImg, resize.NearestNeighbor)); err != nil {
		return user, err
	}
	if err := foo.Flush(); err != nil {
		return user, err
	}
	user.Avatar = base64.StdEncoding.EncodeToString(b.Bytes())
	return user, err
}

func getUUID(username string) (userMinecraft, error) {
	var usr userMinecraft
	url := "https://api.mojang.com/users/profiles/minecraft/" + username
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return userMinecraft{}, err
	}
	req.Header.Add("Accept", `text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8`)
	req.Header.Add("User-Agent", `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11`)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return userMinecraft{}, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return userMinecraft{}, errors.New(http.StatusText(resp.StatusCode))
	}
	if err := json.NewDecoder(resp.Body).Decode(&usr); err != nil {
		return userMinecraft{}, err
	}
	return usr, nil
}

func getUUIDS(usernames []string) ([]userMinecraft, error) {
	var usrs []userMinecraft
	url := "https://api.mojang.com/profiles/minecraft"
	usrsBytes, err := json.Marshal(usernames)
	if err != nil {
		return usrs, err
	}
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(usrsBytes))
	if err != nil {
		return usrs, err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", `text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8`)
	req.Header.Add("User-Agent", `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11`)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return usrs, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return usrs, errors.New(http.StatusText(resp.StatusCode))
	}
	if err := json.NewDecoder(resp.Body).Decode(&usrs); err != nil {
		return usrs, err
	}
	return usrs, nil
}

func getSkinDownloadURL(uuid string) (string, error) {
	usr := struct {
		ID         string
		Name       string
		Properties []struct {
			Name  string
			Value string
		}
	}{}
	url := "https://sessionserver.mojang.com/session/minecraft/profile/" + uuid
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}
	req.Header.Add("Accept", `text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8`)
	req.Header.Add("User-Agent", `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11`)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return "", errors.New(http.StatusText(resp.StatusCode))
	}
	if err := json.NewDecoder(resp.Body).Decode(&usr); err != nil {
		return "", err
	}
	var valueBytes []byte
	for i := range usr.Properties {
		if usr.Properties[i].Name == "textures" {
			valueBytes, err = base64.StdEncoding.DecodeString(usr.Properties[i].Value)
			if err != nil {
				return "", err
			}
		}
	}
	skinStruct := struct {
		Timestamp   int64
		ProfileID   string
		ProfileName string
		Textures    struct {
			Skin struct {
				URL string
			}
		}
	}{}
	if err := json.Unmarshal(valueBytes, &skinStruct); err != nil {
		return "", err
	}
	return skinStruct.Textures.Skin.URL, nil
}
